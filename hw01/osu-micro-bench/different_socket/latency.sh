#!/bin/bash


# Execute job in the partition "lva.q" unless you have special requirements.
#SBATCH --partition=lva
# Name your job to be able to identify it later
#SBATCH --job-name osu_latency_dsockets
# Redirect output stream to this file
#SBATCH --output=/home/cb76/cb761034/thegitrepo/hpc/hw01/osu-micro-bench/different_socket/latency.out
# Maximum number of tasks (=processes) to start in total
#SBATCH --ntasks=2
# Maximum number of tasks (=processes) to start per node
#SBATCH --ntasks-per-node=2
# number of nodes
#SBATCH --nodes=1
# Enforce exclusive node allocation, do not share with other jobs
#SBATCH --exclusive

module load openmpi/4.0.3

BASEPATH='/scratch/c703429/osu-benchmark/libexec/osu-micro-benchmarks/mpi/pt2pt'
EXE="$BASEPATH/osu_latency"

mpiexec -n $SLURM_NTASKS $EXE
