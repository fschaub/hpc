#!/bin/bash

basedir="$1"

bwout="$basedir/bw.out"
latencyout="$basedir/latency.out"

numtrials=10

wd="variance/$basedir"
mkdir -p "$wd/bandwidth"
mkdir -p "$wd/latency"

for i in `seq 1 $numtrials`; do
	sbatch "$basedir/bandwidth.sh"
	sleep 20
	cat "$bwout" | sed 's/^[ #].*$//' | awk 'NF' > "$wd/bandwidth/run_$i.dat"
done


for i in `seq 1 $numtrials`; do
	sbatch "$basedir/latency.sh"
	sleep 20
	cat "$latencyout" | sed 's/^[ #].*$//' | awk 'NF' > "$wd/latency/run_$i.dat"
done

