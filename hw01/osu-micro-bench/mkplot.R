#!/usr/bin/env Rscript

bwcpu<-'different_cpus/bw.out.dat'
bwnode<-'different_nodes/bw.out.dat'
bwsocket<-'different_socket/bw.out.dat'

latencycpu<-'different_cpus/latency.out.dat'
latencynode<-'different_nodes/latency.out.dat'
latencysocket<-'different_socket/latency.out.dat'



bwcpuData<-read.table(bwcpu, header=F)
bwcpuData<-bwcpuData[bwcpuData[,1] > 0,]
bwnodeData<-read.table(bwnode, header=F)
bwnodeData<-bwnodeData[bwnodeData[,1] > 0,]
bwsocketData<-read.table(bwsocket, header=F)
bwsocketData<-bwsocketData[bwsocketData[,1] > 0,]
png('bandwidth.png')
plot(bwcpuData[,1], bwcpuData[,2], xlab="Size", ylab="Bandwidth(MB/s)", type="o", col="blue", lty=1, log="x")

points(bwnodeData[,1], bwnodeData[,2], col="red", pch="*")
lines(bwnodeData[,1], bwnodeData[,2], col="red", lty=2)

points(bwsocketData[,1], bwsocketData[,2], col="black", pch="+")
lines(bwsocketData[,1], bwsocketData[,2], col="black", lty=3)

legend(1,6000,legend=c("different cores","different nodes","different socket"), col=c("blue","red","black"),
                                   pch=c("o","*","+"),lty=c(1,2,3), ncol=1)

dev.off()




latencycpuData<-read.table(latencycpu, header=F)
latencycpuData<-latencycpuData[latencycpuData[,1] > 0,]
latencynodeData<-read.table(latencynode, header=F)
latencynodeData<-latencynodeData[latencynodeData[,1] > 0,]
latencysocketData<-read.table(latencysocket, header=F)
latencysocketData<-latencysocketData[latencysocketData[,1] > 0,]
png('latency.png')
plot(latencycpuData[,1], latencycpuData[,2], xlab="Size", ylab="Latency(us)", type="o", col="blue", lty=1, log="x")

points(latencynodeData[,1], latencynodeData[,2], col="red", pch="*")
lines(latencynodeData[,1], latencynodeData[,2], col="red", lty=2)

points(latencysocketData[,1], latencysocketData[,2], col="black", pch="+")
lines(latencysocketData[,1], latencysocketData[,2], col="black", lty=3)

legend(1,3000,legend=c("different cores","different nodes","different socket"), col=c("blue","red","black"),
                                   pch=c("o","*","+"),lty=c(1,2,3), ncol=1)

dev.off()









warnings()
