#!/bin/bash

for f in `find . -name "*.out"`; do
  cat "$f" | sed 's/^[# ].*$//' | awk 'NF' > "$f.dat"
done
